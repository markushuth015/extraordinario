<?php
class Pais
{
    private $id;
    private $nombre;

    public function __construct($id, $nombre) {
        $this->id = $id;
        $this->nombre = $nombre;
    }

    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }
}

class Cliente
{
    private $id;
    private $paisId;
    private $nombre;

    public function __construct($id, $paisId, $nombre) {
        $this->id = $id;
        $this->paisId = $paisId;
        $this->nombre = $nombre;
    }

    public function getId() {
        return $this->id;
    }

    public function getPaisId() {
        return $this->paisId;
    }

    public function getNombre() {
        return $this->nombre;
    }
}

class Compra
{
    private $id;
    private $clienteId;
    private $montoTotal;
    private $fecha;

    public function __construct($id, $clienteId, $montoTotal, $fecha) {
        $this->id = $id;
        $this->clienteId = $clienteId;
        $this->montoTotal = $montoTotal;
        $this->fecha = $fecha;
    }

    public function getId() {
        return $this->id;
    }

    public function getClienteId() {
        return $this->clienteId;
    }

    public function getMontoTotal() {
        return $this->montoTotal;
    }

    public function getFecha() {
        return $this->fecha;
    }
}

// Instanciar dos compras de un cliente por monto 1.000.000gs y 4.000.000Gs e imprimir
// en pantalla
$pais = new Pais(1, 'Paraguay');
$cliente = new Cliente(1, $pais->getId(), 'Juan Perez');

$compra1 = new Compra(1, $cliente->getId(), 1000000, date('Y-m-d'));
$compra2 = new Compra(2, $cliente->getId(), 4000000, date('Y-m-d'));

echo 'Cliente: ' . $cliente->getNombre() . '<br />';
echo 'Compra 1: Monto: ' . $compra1->getMontoTotal() . ' Fecha: ' . $compra1->getFecha() . '<br />';
echo 'Compra 2: Monto: ' . $compra2->getMontoTotal() . ' Fecha: ' . $compra2->getFecha() . '<br />';