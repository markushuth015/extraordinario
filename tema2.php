<?php 
// Conectar a la base de datos
$db = new PDO('pgsql:host=localhost;dbname=extraordinario', 'postgres', '1234');

// Sacar informacion de libros
$stmt = $db->prepare('SELECT l.titulo, l.anho, l.isbn, e.nombre AS editorial, string_agg(a.nombre, a.apellido) AS autores
                      FROM libros l
                      INNER JOIN editoriales e ON l.editorial_id = e.id
                      INNER JOIN libros_x_autores xa ON l.id = xa.libro_id
                      INNER JOIN autores a ON xa.autor_id = a.id
                      GROUP BY l.id, e.nombre;');
$stmt->execute();

// Desplegar los datos en tablas
echo "<table>";
echo "<tr>
        <th>Titulo</th>
        <th>Año</th>
        <th>ISBN</th>
        <th>Editorial</th>
        <th>Autores</th>
      </tr>";
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    echo "<tr>
            <td>".$row['titulo']."</td>
            <td>".$row['anho']."</td>
            <td>".$row['isbn']."</td>
            <td>".$row['editorial']."</td>
            <td>".$row['autores']."</td>
          </tr>";
}
echo "</table>";
?>